// Import Modules
import { MajiItemSheet } from "./module/item/item-sheet.js";
import { MajiActorSheetBinder } from "./module/actor/binder-sheet.js";
import { MajiActorSheetMonster } from "./module/actor/monster-sheet.js";
import { preloadHandlebarsTemplates } from "./module/preloadTemplates.js";
import { MajiActor } from "./module/actor/entity.js";
import { MajiItem } from "./module/item/entity.js";
import { MAJI } from "./module/config.js";
import * as chat from "./module/chat.js";

// Handlebars template helpers
Handlebars.registerHelper("eq", function (a, b) {
  return a == b;
});

Handlebars.registerHelper("add", function (lh, rh) {
  return parseInt(lh) + parseInt(rh);
});

Handlebars.registerHelper("subtract", function (lh, rh) {
  return parseInt(rh) - parseInt(lh);
});

Handlebars.registerHelper("concat", function (lh, rh) {
  return lh + rh;
});

Handlebars.registerHelper("counter-bar", function (active, mult, value, max) {
  if (active) {
    return mult * value;
  }
  return mult * (max - value);
});

Handlebars.registerHelper('times', function(n, block) {
  var accum = '';
  for(var i = 0; i < n; ++i)
      accum += block.fn(i);
  return accum;
});

Handlebars.registerHelper("actor", function (id, field) {
  const actor = game.actors.entities.find(f => f.id === id);
  return getProperty(actor, field);
});

Handlebars.registerHelper("hasDamage", function (damageObj) {
  if ((damageObj.num && damageObj.die) || damageObj.bonus) {
    return true;
  }
  return false;
});

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {
  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "2d6 + @attributes.speed.value + @attributes.speed.mod",
    decimals: 2,
  };

  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/bleeding.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/blinded.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/burning.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/confused.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/debilitated.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/disoriented.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/frightened.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/frozen.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/poisoned.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/sealed.svg");
  CONFIG.statusEffects.push("/systems/majimonsters/assets/icons/conditions/sleeping.svg");

  CONFIG.MAJI = MAJI;
  CONFIG.Actor.entityClass = MajiActor;
  CONFIG.Item.entityClass = MajiItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("majimonsters", MajiActorSheetBinder, {
    types: ["binder"],
    makeDefault: true,
  });
  Actors.registerSheet("majimonsters", MajiActorSheetMonster, {
    types: ["monster"],
    makeDefault: true,
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("majimonsters", MajiItemSheet, { makeDefault: true });

  await preloadHandlebarsTemplates();
});

/**
 * This function runs after game data has been requested and loaded from the servers, so entities exist
 */
Hooks.once("setup", function () {
  // Localize CONFIG objects once up-front
  const toLocalize = ["elements", "conditions", "reach", "descriptors", "actions", "sizes", "stats", "attributes", "skills"];
  for (let o of toLocalize) {
    CONFIG.MAJI[o] = Object.entries(CONFIG.MAJI[o]).reduce((obj, e) => {
      obj[e[0]] = game.i18n.localize(e[1]);
      return obj;
    }, {});
  }
});

Hooks.on("renderChatMessage", (app, html, data) => {
  // Highlight critical success or failure die
  chat.highlightCriticalSuccessFailure(app, html, data);
});

Hooks.on("getChatLogEntryContext", chat.addChatMessageContextOptions);
Hooks.on("renderChatLog", (app, html, data) => MajiItem.chatListeners(html));