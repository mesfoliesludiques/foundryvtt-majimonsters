export class MajiActor extends Actor {
  /**
   * Extends data from base Actor class
   */
  prepareData() {
    super.prepareData();
    return this.data;
  }
  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
    /* -------------------------------------------- */
  /** @override */
  async update(data, options = {}) {
    // Calculate affinity
    if (this.data.type == "monster" && data["data.details.grade"]) {
      data["data.affinity.value"] = 2 * data["data.details.grade"];
      data["data.resistance.value"] = 2 * data["data.details.grade"];
    }
    return super.update(data, options);
  }
  /* -------------------------------------------- */
  /** @override */
  async createOwnedItem(itemData, options) {
    return super.createOwnedItem(itemData, options);
  }
  /* -------------------------------------------- */
  /*  Rolls                                       */
  /* -------------------------------------------- */
  counterIncrement(featId, delta) {
    let items = this.items.filter((i) => i.id == featId);
    items[0].data["data"]["counter"].value += delta;
  }

  rollSkill(skillId, options = {}) {
    const label = CONFIG.MAJI.skills[skillId];

    const abl = this.data.data.skills[skillId];
    let parts = [];
    if (abl.value <= 4) {
      parts.push("2d4");
    } else if (abl.value <= 7) {
      parts.push("2d6");
    } else {
      parts.push("2d8");
    }

    let rollMode = game.settings.get("core", "rollMode");
    let roll = new Roll(parts.join(" + "), {}).roll();
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: `${label} Skill Test`,
      },
      { rollMode }
    );
    return roll;
  }

  rollStat(statId, options = {}) {
    const label = CONFIG.MAJI.stats[statId];
    let parts = [];
    if (options.empowered) {
      parts.push("2d8");
    } else {
      parts.push("2d6");
    }
    parts.push(`${this.data.data.attributes[statId].value}`);
    let rollMode = game.settings.get("core", "rollMode");
    let roll = new Roll(parts.join(" + "), {}).roll();
    // Critical / fumble
    if (roll.parts[0].total >= 11) {
      roll.parts[0].options.critical = true;
    } else if (roll.parts[0].total == 2) {
      roll.parts[0].options.fumble = true;
    }
    if (roll.parts[0].total == 2 * roll.parts[0].faces) {
      roll.parts[0].options.perfect = true;
    }
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: `${label} Attribute Test`,
      },
      { rollMode }
    );
    return roll;
  }

  rollAttribute(attributeId, options = {}) {
    const label = CONFIG.MAJI.attributes[attributeId];

    const abl = this.data.data.attributes[attributeId];
    let parts = [];
    if (abl.value <= 4) {
      parts.push("2d4");
    } else if (abl.value <= 7) {
      parts.push("2d6");
    } else {
      parts.push("2d8");
    }
    let rollMode = game.settings.get("core", "rollMode");
    let roll = new Roll(parts.join(" + "), {}).roll();
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: `${label} Attribute Test`,
      },
      { rollMode }
    );
    return roll;
  }

  rollBinding(options = {}) {
    let rollMode = game.settings.get("core", "rollMode");
    let roll = new Roll("d20", {}).roll();
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: `${game.i18n.localize("MAJI.check.bind")}`,
      },
      { rollMode }
    );
    return roll;
  }

  rollInit(monsterId, options = {}) {
    let monster = game.actors.get(monsterId);
    let speed = monster.data.data.attributes.speed.value + monster.data.data.attributes.speed.mod;
    if (!game.combats.active) return;
    let combatant = game.combats.active.getCombatant(this.actor);
    console.log(combatant);
  }

  rollTrigger({ threshold = 0, condition = "" }, options = {}) {
    let rollMode = game.settings.get("core", "rollMode");
    let roll = new Roll("1d6", {}).roll();
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: `${
          CONFIG.MAJI.conditions[condition]
        }(${threshold}) ${game.i18n.localize("MAJI.technique.trigger")}`,
      },
      { rollMode }
    );
    return roll;
  }

  static async applyDamage(roll, options = {}) {
    let value = Math.floor(parseFloat(roll.find(".dice-total").text()));
    const promises = [];
    for (let t of canvas.tokens.controlled) {
      let a = t.actor,
        hp = a.data.data.hp;
      let delta = 0;
      if (a.data.type == "monster") {
        if (options.vulnerable) {
          delta -= value + a.data.data.affinity.value;
        } else if (options.resistant) {
          delta -= Math.max(0, value - a.data.data.resistance.value);
        } else if (options.healing) {
          delta += value;
        } else {
          delta -= value;
        }
      } else {
        delta -= options.healing ? -value : value;
      }
      promises.push(
        t.actor.update({
          "data.hp.value": Math.clamped(hp.value + delta, 0, hp.max),
        })
      );
    }
    return Promise.all(promises);
  }
}
