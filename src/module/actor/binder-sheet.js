import { MajiActor } from "./entity.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 */
export class MajiActorSheetBinder extends ActorSheet {
  constructor(...args) {
    super(...args);
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["majimonsters", "sheet", "binder", "actor"],
      template: "systems/majimonsters/templates/actors/binder-sheet.html",
      width: 660,
      height: 670,
      resizable: false,
      tabs: [
        {
          navSelector: ".tabs",
          contentSelector: ".sheet-body",
          initial: "abilities",
        },
      ],
    });
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    const data = super.getData();
    
    data.config = CONFIG.MAJI;

    for (let [s, skill] of Object.entries(data.data.skills)) {
      data.data.skills[s].label = game.i18n.localize(`MAJI.skill.${s}`);
    }
    for (let [a, attribute] of Object.entries(data.data.attributes)) {
      data.data.attributes[a].label = game.i18n.localize(`MAJI.attribute.${a}`);
    }
    // Prepare owned items
    this._prepareItems(data);

    // DEBUG
    return data;
  }

  /**
   * Organize and classify Owned Items for Character sheets
   * @private
   */
  _prepareItems(data) {
    // Partition items by category
    let [inventory, features, drajules] = data.items.reduce(
      (arr, item) => {
        // Classify items into types
        if (item.type === "item") arr[0].push(item);
        else if (item.type === "feature") arr[1].push(item);
        else if (item.type === "drajule") arr[2].push(item);
        return arr;
      },
      [[], [], [], []]
    );

    // Assign and return
    data.inventory = inventory;
    data.drajules = drajules;
    data.features = features;
  }

  /* -------------------------------------------- */

  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item-entry"),
      expanded = !li.children(".collapsible").hasClass("collapsed");
    li = $(li);
    let ol = li.children(".collapsible");
    let icon = li.find("i.fas");

    // Collapse the Playlist
    if (expanded) {
      ol.slideUp(200, () => {
        ol.addClass("collapsed");
        icon.removeClass("fa-angle-up").addClass("fa-angle-down");
      });
    }

    // Expand the Playlist
    else {
      ol.slideDown(200, () => {
        ol.removeClass("collapsed");
        icon.removeClass("fa-angle-down").addClass("fa-angle-up");
      });
    }
  }

  _onRollAttribute(event) {
    event.preventDefault();
    let attribute = event.currentTarget.dataset.attribute;
    this.actor.rollAttribute(attribute, { event: event });
  }

  _onRollSkill(event) {
    event.preventDefault();
    let skill = event.currentTarget.dataset.skill;
    this.actor.rollSkill(skill, { event: event });
  }

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find(".item-edit").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find(".item-delete").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    html.find(".item-create").click((event) => {
      event.preventDefault();
      const header = event.currentTarget;
      const type = header.dataset.type;
      const itemData = {
        name: `New ${type.capitalize()}`,
        type: type,
        data: duplicate(header.dataset),
      };
      delete itemData.data["type"];
      return this.actor.createOwnedItem(itemData);
    });

    // increment counter
    html.find(".counterbar").click((event) => {
      let actorObject = this.actor;
      let delta = -1;
      let element = event.currentTarget;
      if (element.classList.contains("inactive")) {
        delta = 1;
      }
      let item = element.parentElement.dataset.itemId;
      actorObject.counterIncrement(item, delta);
      this.render();
    });

    html.find(".item-name").click((event) => {
      this._onItemSummary(event);
    });

    html.find(".skill-roll").click((event) => {
      this._onRollSkill(event);
    });
    html.find(".attribute-roll").click((event) => {
      this._onRollAttribute(event);
    });

    html.find(".entity").click(event => {
      event.preventDefault();
      const element = event.currentTarget;
      const entityId = element.dataset.entityId;
      const entity = game.actors.entities.find(f => f.id === entityId);
      const sheet = entity.sheet;
      if (sheet._minimized) return sheet.maximize();
      else return sheet.render(true);
    });

    html.find(".binding-roll").click(event => {
      this.actor.rollBinding({ event: event });
    });

    // Handle default listeners last so system listeners are triggered first
    super.activateListeners(html);
  }
}
