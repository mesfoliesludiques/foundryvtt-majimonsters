import { MajiActor } from "./actor/entity.js";

/**
 * Highlight critical success or failure on d20 rolls
 */
export const highlightCriticalSuccessFailure = function (message, html, data) {
  if (!message.isRoll || !message.isRollVisible || !message.roll.parts.length)
    return;

  let d = message.roll.parts[0];
  // Highlight successes and failures
  if (d.options.critical || d.options.perfect)
    html.find(".dice-total").addClass("success");
  else if (d.options.fumble) html.find(".dice-total").addClass("failure");
};

/* -------------------------------------------- */

/**
 * This function is used to hook into the Chat Log context menu to add additional options to each message
 * These options make it easy to conveniently apply damage to controlled tokens based on the value of a Roll
 *
 * @param {HTMLElement} html    The Chat Message being rendered
 * @param {Array} options       The Array of Context Menu options
 *
 * @return {Array}              The extended options Array including new context choices
 */
export const addChatMessageContextOptions = function (html, options) {
  let canApply = (li) =>
    canvas.tokens.controlledTokens.length && li.find(".dice-roll").length;
  options.push(
    {
      name: "Apply Damage",
      icon: '<i class="fas fa-user-minus"></i>',
      condition: canApply,
      callback: (li) => MajiActor.applyDamage(li),
    },
    {
      name: "Apply Healing",
      icon: '<i class="fas fa-user-plus"></i>',
      condition: canApply,
      callback: (li) => MajiActor.applyDamage(li, { healing: true }),
    },
    {
      name: "Resistant Damage",
      icon: '<i class="fas fa-user-shield"></i>',
      condition: canApply,
      callback: (li) => MajiActor.applyDamage(li, { resistant: true }),
    },
    {
      name: "Vulnerable Damage",
      icon: '<i class="fas fa-user-injured"></i>',
      condition: canApply,
      callback: (li) => MajiActor.applyDamage(li, { vulnerable: true }),
    }
  );
  return options;
};
