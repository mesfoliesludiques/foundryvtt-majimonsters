export const preloadHandlebarsTemplates = async function () {
    const templatePaths = [
        //Character Sheets
        'systems/majimonsters/templates/actors/binder-html.html',
        'systems/majimonsters/templates/actors/monster-html.html',
        //Actor partials
        //Sheet tabs
        'systems/majimonsters/templates/actors/partials/binder-header.html',
        'systems/majimonsters/templates/actors/partials/binder-abilities-tab.html',
        'systems/majimonsters/templates/actors/partials/binder-inventory-tab.html',

        'systems/majimonsters/templates/actors/partials/monster-header.html',
        'systems/majimonsters/templates/actors/partials/monster-attributes-tab.html',
        'systems/majimonsters/templates/actors/partials/monster-techniques-tab.html',
        'systems/majimonsters/templates/actors/partials/monster-traits-tab.html'
    ];
    return loadTemplates(templatePaths);
};
