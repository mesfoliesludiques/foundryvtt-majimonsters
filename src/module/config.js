export const MAJI = {};

MAJI.elements = {
  basic: "MAJI.element.basic",
  earth: "MAJI.element.earth",
  fire: "MAJI.element.fire",
  fury: "MAJI.element.fury",
  ice: "MAJI.element.ice",
  lightning: "MAJI.element.lightning",
  mystic: "MAJI.element.mystic",
  verdant: "MAJI.element.verdant",
  water: "MAJI.element.water",
  wind: "MAJI.element.wind",
};

MAJI.conditions = {
  bleeding: "MAJI.condition.bleeding",
  blinded: "MAJI.condition.blinded",
  burning: "MAJI.condition.burning",
  confused: "MAJI.condition.confused",
  debilitated: "MAJI.condition.debilitated",
  disoriented: "MAJI.condition.disoriented",
  frightened: "MAJI.condition.frightened",
  frozen: "MAJI.condition.frozen",
  grabbed: "MAJI.condition.grabbed",
  grounded: "MAJI.condition.grounded",
  outnumbered: "MAJI.condition.grounded",
  poisoned: "MAJI.condition.poisoned",
  sealed: "MAJI.condition.sealed",
  sleeping: "MAJI.condition.sleeping",
};

MAJI.reach = {
  melee: "MAJI.reach.melee",
  ranged: "MAJI.reach.ranged",
  zone: "MAJI.reach.zone",
  aura: "MAJI.reach.aura",
  area: "MAJI.reach.area",
  sphere: "MAJI.reach.sphere",
  line: "MAJI.reach.line",
  cone: "MAJI.reach.cone",
  wall: "MAJI.reach.wall",
};

MAJI.descriptors = {
  bite: "MAJI.descriptor.bite",
  breath: "MAJI.descriptor.breath",
  claw: "MAJI.descriptor.claw",
  gaze: "MAJI.descriptor.gaze",
  grit: "MAJI.descriptor.grit",
  healing: "MAJI.descriptor.healing",
  horn: "MAJI.descriptor.horn",
  recoil: "MAJI.descriptor.recoil",
  tail: "MAJI.descriptor.tail",
  wing: "MAJI.descriptor.wing",
};

MAJI.actions = {
  combat: "MAJI.action.combat",
  utility: "MAJI.action.utility",
  response: "MAJI.action.response",
  move: "MAJI.action.move",
};

MAJI.sizes = {
  tiny: "MAJI.size.tiny",
  small: "MAJI.size.small",
  medium: "MAJI.size.medium",
  large: "MAJI.size.large",
  huge: "MAJI.size.huge",
};

MAJI.stats = {
  health: "MAJI.stats.health",
  strike: "MAJI.stats.strike",
  magic: "MAJI.stats.magic",
  brawn: "MAJI.stats.brawn",
  speed: "MAJI.stats.speed",
  protection: "MAJI.stats.protection",
  discipline: "MAJI.stats.discipline",
  talent: "MAJI.stats.talent",
};

MAJI.skills = {
  guile: "MAJI.skill.guile",
  perception: "MAJI.skill.perception",
  persuasion: "MAJI.skill.persuasion",
  wits: "MAJI.skill.wits",
  education: "MAJI.skill.education",
  intuition: "MAJI.skill.intuition",
  proficiency: "MAJI.skill.proficiency",
  survival: "MAJI.skill.survival",
  athletics: "MAJI.skill.athletics",
  fortitude: "MAJI.skill.fortitude",
  strength: "MAJI.skill.strength",
  willpower: "MAJI.skill.willpower",
};

MAJI.attributes = {
  interaction: "MAJI.attribute.interaction",
  knowledge: "MAJI.attribute.knowledge",
  vigor: "MAJI.attribute.vigor"
};
